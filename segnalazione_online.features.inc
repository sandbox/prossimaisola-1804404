<?php

/**
 * Implementation of hook_ctools_plugin_api().
 */
function segnalazione_online_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => 1);
  }
}

/**
 * Implementation of hook_node_info().
 */
function segnalazione_online_node_info() {
  $items = array(
    'richiesta' => array(
      'name' => t('Richiesta'),
      'module' => 'features',
      'description' => t('Questo tipo di contenuto permette di inviare alle associazione dei consumatori una richiesta di assistenza'),
      'has_title' => '1',
      'title_label' => t('Titolo'),
      'has_body' => '1',
      'body_label' => t('Richiesta'),
      'min_word_count' => '25',
      'help' => '',
    ),
    'risposta' => array(
      'name' => t('Risposta'),
      'module' => 'features',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Titolo'),
      'has_body' => '1',
      'body_label' => t('Risposta'),
      'min_word_count' => '0',
      'help' => '',
    ),
  );
  return $items;
}

/**
 * Implementation of hook_views_api().
 */
function segnalazione_online_views_api() {
  return array(
    'api' => '2',
  );
}
