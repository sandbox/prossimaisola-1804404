<?php

/**
 * Implementation of hook_content_default_fields().
 */
function segnalazione_online_content_default_fields() {
  $fields = array();

  // Exported field: field_datarichiesta
  $fields['richiesta-field_datarichiesta'] = array(
    'field_name' => 'field_datarichiesta',
    'type_name' => 'richiesta',
    'display_settings' => array(
      'label' => array(
        'format' => 'above',
        'exclude' => 0,
      ),
      '5' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '2' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '3' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'email_plain' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'email_html' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'date',
    'required' => '0',
    'multiple' => '0',
    'module' => 'date',
    'active' => '1',
    'granularity' => array(
      'year' => 'year',
      'month' => 'month',
      'day' => 'day',
      'hour' => 'hour',
      'minute' => 'minute',
      'second' => 'second',
    ),
    'timezone_db' => 'UTC',
    'tz_handling' => 'site',
    'todate' => '',
    'repeat' => 0,
    'repeat_collapsed' => '',
    'default_format' => 'medium',
    'widget' => array(
      'default_value' => 'strtotime',
      'default_value_code' => '-2hours',
      'default_value2' => 'same',
      'default_value_code2' => '',
      'input_format' => 'Y-m-d\\TH:i:s',
      'input_format_custom' => '',
      'increment' => '1',
      'text_parts' => array(),
      'year_range' => '-3:+3',
      'label_position' => 'above',
      'label' => 'Data richiesta',
      'weight' => '7',
      'description' => '',
      'type' => 'date_popup',
      'module' => 'date',
    ),
  );

  // Exported field: field_richiesta_attribuzione_ass
  $fields['richiesta-field_richiesta_attribuzione_ass'] = array(
    'field_name' => 'field_richiesta_attribuzione_ass',
    'type_name' => 'richiesta',
    'display_settings' => array(
      'label' => array(
        'format' => 'above',
        'exclude' => 0,
      ),
      '5' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '2' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '3' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'email_plain' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'email_html' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'number_integer',
    'required' => '1',
    'multiple' => '0',
    'module' => 'number',
    'active' => '1',
    'prefix' => '',
    'suffix' => '',
    'min' => '',
    'max' => '',
    'allowed_values' => '',
    'allowed_values_php' => '$associazioni = _attribuzione_automatica_get_associazioni();
$_associazioni = array();
foreach($associazioni as $associazione){
	$_associazioni[$associazione->nid] = $associazione->node_title;
}
$associazioni = $_associazioni;
return $associazioni;',
    'widget' => array(
      'default_value' => NULL,
      'default_value_php' => '$nid_turn = _attribuzione_automatica_get_turn();
return array(
	0 => array(\'value\' => $nid_turn)
);',
      'label' => 'Attribuzione Automatica Associazione',
      'weight' => '8',
      'description' => '',
      'type' => 'optionwidgets_select',
      'module' => 'optionwidgets',
    ),
  );

  // Exported field: field_risposta_rilevazione_refer
  $fields['risposta-field_risposta_rilevazione_refer'] = array(
    'field_name' => 'field_risposta_rilevazione_refer',
    'type_name' => 'risposta',
    'display_settings' => array(
      'weight' => '-4',
      'parent' => '',
      'label' => array(
        'format' => 'above',
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 1,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 1,
      ),
      '5' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '2' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '3' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'email_plain' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'email_html' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'nodereference',
    'required' => '1',
    'multiple' => '0',
    'module' => 'nodereference',
    'active' => '1',
    'referenceable_types' => array(
      'scheda_problemi' => 'scheda_problemi',
      'alert' => 0,
      'anagrafe_controparte' => 0,
      'argomenti_immagini' => 0,
      'articolo' => 0,
      'associazione' => 0,
      'book' => 0,
      'casetracker_basic_case' => 0,
      'domanda' => 0,
      'faq' => 0,
      'event' => 0,
      'feed' => 0,
      'feed_item' => 0,
      'group' => 0,
      'home_categoria' => 0,
      'image' => 0,
      'fattispecie_particolare' => 0,
      'landingpage' => 0,
      'blog' => 0,
      'simplenews' => 0,
      'newsfeed' => 0,
      'page' => 0,
      'panel' => 0,
      'profile' => 0,
      'casetracker_basic_project' => 0,
      'richiesta' => 0,
      'risposta' => 0,
      'risposta_associazione' => 0,
      'scheda_associazione' => 0,
      'scheda_approfondimento' => 0,
      'scheda_didattica' => 0,
      'home_statica' => 0,
      'feed_ical_item' => 0,
      'feed_ical' => 0,
    ),
    'advanced_view' => '--',
    'advanced_view_args' => '',
    'widget' => array(
      'node_link' => array(
        'teaser' => 0,
        'full' => 1,
        'title' => 'Crea una risposta',
        'hover_title' => 'Crea una Risposta al quesito dell\'utente',
        'destination' => 'source',
      ),
      'fallback' => 'page_not_found',
      'edit_fallback' => 0,
      'label' => 'Scheda Rilevazione Reference',
      'weight' => '-4',
      'description' => '',
      'type' => 'nodereference_url',
      'module' => 'nodereference_url',
    ),
  );

  // Exported field: field_richiesta
  $fields['risposta_associazione-field_richiesta'] = array(
    'field_name' => 'field_richiesta',
    'type_name' => 'risposta_associazione',
    'display_settings' => array(
      'label' => array(
        'format' => 'above',
        'exclude' => 0,
      ),
      '5' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '2' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '3' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'nodereference',
    'required' => '1',
    'multiple' => '0',
    'module' => 'nodereference',
    'active' => '1',
    'referenceable_types' => array(
      'richiesta' => 'richiesta',
      'articolo' => 0,
      'associazione' => 0,
      'book' => 0,
      'domanda' => 0,
      'faq' => 0,
      'event' => 0,
      'feed' => 0,
      'feed_item' => 0,
      'feedapi_node' => 0,
      'group' => 0,
      'home_categoria' => 0,
      'image' => 0,
      'fattispecie_particolare' => 0,
      'blog' => 0,
      'newsfeed' => 0,
      'page' => 0,
      'panel' => 0,
      'profile' => 0,
      'feed_reader' => 0,
      'risposta_associazione' => 0,
      'scheda_approfondimento' => 0,
      'scheda_didattica' => 0,
      'scheda_problemi' => 0,
      'home_statica' => 0,
      'feed_ical_item' => 0,
      'feed_ical' => 0,
    ),
    'advanced_view' => '--',
    'advanced_view_args' => '',
    'widget' => array(
      'node_link' => array(
        'teaser' => 0,
        'full' => 1,
        'title' => 'Invia risposta',
        'hover_title' => 'Invia una risposta completa di documentazione',
        'destination' => 'default',
      ),
      'fallback' => 'autocomplete',
      'edit_fallback' => 0,
      'label' => 'Richiesta',
      'weight' => '-4',
      'description' => '',
      'type' => 'nodereference_autocomplete',
      'module' => 'nodereference',
    ),
  );

  // Exported field: field_scheda_problema_richie
  $fields['scheda_problemi-field_scheda_problema_richie'] = array(
    'field_name' => 'field_scheda_problema_richie',
    'type_name' => 'scheda_problemi',
    'display_settings' => array(
      'label' => array(
        'format' => 'above',
        'exclude' => 0,
      ),
      '5' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '2' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '3' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'email_plain' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'email_html' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'nodereference',
    'required' => '0',
    'multiple' => '0',
    'module' => 'nodereference',
    'active' => '1',
    'referenceable_types' => array(
      'richiesta' => 'richiesta',
      'alert' => 0,
      'anagrafe_controparte' => 0,
      'argomenti_immagini' => 0,
      'articolo' => 0,
      'associazione' => 0,
      'book' => 0,
      'casetracker_basic_case' => 0,
      'domanda' => 0,
      'faq' => 0,
      'event' => 0,
      'feed' => 0,
      'feed_item' => 0,
      'group' => 0,
      'home_categoria' => 0,
      'image' => 0,
      'fattispecie_particolare' => 0,
      'landingpage' => 0,
      'blog' => 0,
      'simplenews' => 0,
      'newsfeed' => 0,
      'page' => 0,
      'panel' => 0,
      'profile' => 0,
      'casetracker_basic_project' => 0,
      'risposta_associazione' => 0,
      'scheda_associazione' => 0,
      'scheda_approfondimento' => 0,
      'scheda_didattica' => 0,
      'scheda_problemi' => 0,
      'home_statica' => 0,
      'feed_ical_item' => 0,
      'feed_ical' => 0,
    ),
    'advanced_view' => '--',
    'advanced_view_args' => '',
    'widget' => array(
      'node_link' => array(
        'teaser' => 0,
        'full' => 1,
        'title' => 'Crea una scheda rilevazione',
        'hover_title' => 'Crea una scheda rilevazione',
        'destination' => 'default',
      ),
      'fallback' => 'autocomplete',
      'edit_fallback' => 0,
      'multistep' => '1',
      'label' => 'Referenza Richiesta',
      'weight' => '-7',
      'description' => '',
      'type' => 'nodereference_url',
      'module' => 'nodereference_url',
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Attribuzione Automatica Associazione');
  t('Data richiesta');
  t('Referenza Richiesta');
  t('Richiesta');
  t('Scheda Rilevazione Reference');

  return $fields;
}
