<?php

/**
 * Implementation of hook_strongarm().
 */
function segnalazione_online_strongarm() {
  $export = array();
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'ant_risposta';
  $strongarm->value = '1';

  $export['ant_risposta'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'content_extra_weights_richiesta';
  $strongarm->value = array(
    'title' => '-5',
    'body_field' => '-3',
    'revision_information' => '1',
    'author' => '2',
    'options' => '3',
    'comment_settings' => '5',
    'menu' => '-4',
    'book' => '-1',
    'path' => '6',
    'print' => '4',
    'allega_risposta_node_content_1' => '-2',
    'workflow' => '0',
  );

  $export['content_extra_weights_richiesta'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'content_extra_weights_risposta';
  $strongarm->value = array(
    'title' => '-5',
    'body_field' => '-2',
    'revision_information' => '3',
    'author' => '2',
    'options' => '4',
    'comment_settings' => '8',
    'menu' => '-1',
    'book' => '1',
    'path' => '7',
    'attachments' => '6',
    'print' => '5',
    'workflow' => '0',
  );

  $export['content_extra_weights_risposta'] = $strongarm;
  return $export;
}
