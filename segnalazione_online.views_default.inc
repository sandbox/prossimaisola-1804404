<?php

/**
 * Implementation of hook_views_default_views().
 */
function segnalazione_online_views_default_views() {
  $views = array();

  // Exported view: allega_risposta
  $view = new view;
  $view->name = 'allega_risposta';
  $view->description = '';
  $view->tag = 'Segnalazione online > Allega risposta';
  $view->view_php = '';
  $view->base_table = 'node';
  $view->is_cacheable = FALSE;
  $view->api_version = 2;
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
  $handler = $view->new_display('default', 'Defaults', 'default');
  $handler->override_option('relationships', array(
    'field_risposta_rilevazione_refer_nid' => array(
      'label' => 'Scheda Rilevazione Reference',
      'required' => 0,
      'delta' => -1,
      'id' => 'field_risposta_rilevazione_refer_nid',
      'table' => 'node_data_field_risposta_rilevazione_refer',
      'field' => 'field_risposta_rilevazione_refer_nid',
      'override' => array(
        'button' => 'Override',
      ),
      'relationship' => 'none',
    ),
    'field_scheda_problema_richie_nid_1' => array(
      'label' => 'Referenza Richiesta',
      'required' => 0,
      'delta' => -1,
      'id' => 'field_scheda_problema_richie_nid_1',
      'table' => 'node_data_field_scheda_problema_richie',
      'field' => 'field_scheda_problema_richie_nid',
      'override' => array(
        'button' => 'Override',
      ),
      'relationship' => 'field_risposta_rilevazione_refer_nid',
    ),
  ));
  $handler->override_option('fields', array(
    'body' => array(
      'label' => '',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '_blank',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
        'strip_tags' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'exclude' => 0,
      'id' => 'body',
      'table' => 'node_revisions',
      'field' => 'body',
      'relationship' => 'none',
    ),
    'field_risposta_gradimento_rating' => array(
      'label' => 'Gradimento risposta',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
        'strip_tags' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'link_to_node' => 0,
      'label_type' => 'widget',
      'format' => 'default',
      'multiple' => array(
        'group' => TRUE,
        'multiple_number' => '',
        'multiple_from' => '',
        'multiple_reversed' => FALSE,
      ),
      'exclude' => 0,
      'id' => 'field_risposta_gradimento_rating',
      'table' => 'node_data_field_risposta_gradimento',
      'field' => 'field_risposta_gradimento_rating',
      'override' => array(
        'button' => 'Override',
      ),
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('arguments', array(
    'nid' => array(
      'default_action' => 'default',
      'style_plugin' => 'default_summary',
      'style_options' => array(),
      'wildcard' => 'all',
      'wildcard_substitution' => 'All',
      'title' => '',
      'breadcrumb' => '',
      'default_argument_type' => 'node',
      'default_argument' => '',
      'validate_type' => 'none',
      'validate_fail' => 'not found',
      'break_phrase' => 0,
      'not' => 0,
      'id' => 'nid',
      'table' => 'node',
      'field' => 'nid',
      'validate_user_argument_type' => 'uid',
      'validate_user_roles' => array(
        '2' => 0,
        '14' => 0,
        '9' => 0,
        '11' => 0,
        '12' => 0,
        '13' => 0,
        '5' => 0,
        '16' => 0,
        '10' => 0,
        '4' => 0,
        '3' => 0,
        '15' => 0,
        '6' => 0,
        '8' => 0,
        '7' => 0,
      ),
      'relationship' => 'field_scheda_problema_richie_nid_1',
      'default_options_div_prefix' => '',
      'default_argument_fixed' => '',
      'default_argument_user' => 0,
      'default_argument_image_size' => '_original',
      'default_argument_php' => '',
      'validate_argument_node_type' => array(
        'blog' => 0,
        'event' => 0,
        'feed_ical' => 0,
        'feed_ical_item' => 0,
        'group' => 0,
        'profile' => 0,
        'feed' => 0,
        'feed_item' => 0,
        'image' => 0,
        'argomenti_immagini' => 0,
        'anagrafe_controparte' => 0,
        'articolo' => 0,
        'faq' => 0,
        'fattispecie_particolare' => 0,
        'newsfeed' => 0,
        'scheda_approfondimento' => 0,
        'scheda_didattica' => 0,
        'scheda_problemi' => 0,
        'casetracker_basic_case' => 0,
        'casetracker_basic_project' => 0,
        'panel' => 0,
        'alert' => 0,
        'associazione' => 0,
        'book' => 0,
        'domanda' => 0,
        'home_categoria' => 0,
        'home_statica' => 0,
        'landingpage' => 0,
        'page' => 0,
        'richiesta' => 0,
        'risposta' => 0,
        'risposta_associazione' => 0,
        'scheda_associazione' => 0,
        'simplenews' => 0,
      ),
      'validate_argument_node_access' => 0,
      'validate_argument_nid_type' => 'nid',
      'validate_argument_vocabulary' => array(
        '26' => 0,
        '27' => 0,
        '30' => 0,
        '10' => 0,
        '23' => 0,
        '21' => 0,
        '9' => 0,
        '6' => 0,
        '12' => 0,
        '11' => 0,
        '25' => 0,
        '24' => 0,
        '29' => 0,
        '31' => 0,
        '28' => 0,
      ),
      'validate_argument_type' => 'tid',
      'validate_argument_transform' => 0,
      'validate_user_restrict_roles' => 0,
      'validate_argument_node_flag_name' => '*relationship*',
      'validate_argument_node_flag_test' => 'flaggable',
      'validate_argument_node_flag_id_type' => 'id',
      'validate_argument_user_flag_name' => '*relationship*',
      'validate_argument_user_flag_test' => 'flaggable',
      'validate_argument_user_flag_id_type' => 'id',
      'image_size' => array(
        '_original' => '_original',
        'thumbnail' => 'thumbnail',
        'preview' => 'preview',
      ),
      'validate_argument_is_member' => 'OG_VIEWS_DO_NOT_VALIDATE_MEMBERSHIP',
      'validate_argument_group_node_type' => array(
        'group' => 0,
        'associazione' => 0,
      ),
      'validate_argument_php' => '',
    ),
  ));
  $handler->override_option('filters', array(
    'type' => array(
      'operator' => 'in',
      'value' => array(
        'risposta' => 'risposta',
      ),
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'id' => 'type',
      'table' => 'node',
      'field' => 'type',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('access', array(
    'type' => 'atrium_feature',
    'spaces_feature' => '0',
    'perm' => 'access content',
  ));
  $handler->override_option('cache', array(
    'type' => 'none',
  ));
  $handler->override_option('title', 'Risposta');
  $handler->override_option('header_format', '3');
  $handler->override_option('header_empty', 0);
  $handler->override_option('empty', 'Nessuna risposta è stata ancora creata');
  $handler->override_option('empty_format', '1');
  $handler->override_option('items_per_page', 1);
  $handler->override_option('row_plugin', 'node');
  $handler->override_option('row_options', array(
    'relationship' => 'none',
    'build_mode' => 'full',
    'links' => 0,
    'comments' => 0,
  ));
  $handler = $view->new_display('node_content', 'Node content', 'node_content_1');
  $handler->override_option('types', array(
    'richiesta' => 'richiesta',
  ));
  $handler->override_option('modes', array(
    '0' => 'full',
  ));
  $handler->override_option('argument_mode', 'nid');
  $handler->override_option('default_argument', '');
  $handler->override_option('show_title', 0);

  $views[$view->name] = $view;

  // Exported view: richieste_risposte_utente
  $view = new view;
  $view->name = 'richieste_risposte_utente';
  $view->description = '';
  $view->tag = 'Segnalazione online > Richieste e risposte utente';
  $view->view_php = '';
  $view->base_table = 'node';
  $view->is_cacheable = FALSE;
  $view->api_version = 2;
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
  $handler = $view->new_display('default', 'Defaults', 'default');
  $handler->override_option('fields', array(
    'title' => array(
      'label' => 'Titolo',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
        'strip_tags' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'link_to_node' => 0,
      'exclude' => 1,
      'id' => 'title',
      'table' => 'node',
      'field' => 'title',
      'relationship' => 'none',
    ),
    'body' => array(
      'label' => 'Anteprima',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 1,
        'max_length' => '60',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 1,
        'strip_tags' => 1,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'exclude' => 0,
      'id' => 'body',
      'table' => 'node_revisions',
      'field' => 'body',
      'relationship' => 'none',
    ),
    'created' => array(
      'label' => 'Data',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
        'strip_tags' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'date_format' => 'small',
      'custom_date_format' => '',
      'exclude' => 0,
      'id' => 'created',
      'table' => 'node',
      'field' => 'created',
      'relationship' => 'none',
    ),
    'state' => array(
      'label' => 'Stato',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
        'strip_tags' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'exclude' => 0,
      'id' => 'state',
      'table' => 'workflow_states',
      'field' => 'state',
      'relationship' => 'none',
    ),
    'view_node' => array(
      'label' => 'Link',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
        'strip_tags' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'text' => '',
      'exclude' => 0,
      'id' => 'view_node',
      'table' => 'node',
      'field' => 'view_node',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('sorts', array(
    'created' => array(
      'order' => 'DESC',
      'granularity' => 'second',
      'id' => 'created',
      'table' => 'node',
      'field' => 'created',
      'override' => array(
        'button' => 'Override',
      ),
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('arguments', array(
    'uid' => array(
      'default_action' => 'default',
      'style_plugin' => 'default_summary',
      'style_options' => array(),
      'wildcard' => 'all',
      'wildcard_substitution' => 'All',
      'title' => '',
      'breadcrumb' => '',
      'default_argument_type' => 'user',
      'default_argument' => '',
      'validate_type' => 'none',
      'validate_fail' => 'not found',
      'break_phrase' => 0,
      'not' => 0,
      'id' => 'uid',
      'table' => 'users',
      'field' => 'uid',
      'validate_user_argument_type' => 'uid',
      'validate_user_roles' => array(
        '2' => 0,
        '14' => 0,
        '9' => 0,
        '11' => 0,
        '12' => 0,
        '13' => 0,
        '5' => 0,
        '16' => 0,
        '10' => 0,
        '4' => 0,
        '3' => 0,
        '15' => 0,
        '6' => 0,
        '8' => 0,
        '7' => 0,
      ),
      'relationship' => 'none',
      'default_options_div_prefix' => '',
      'default_argument_fixed' => '',
      'default_argument_user' => 0,
      'default_argument_image_size' => '_original',
      'default_argument_php' => '',
      'validate_argument_node_type' => array(
        'blog' => 0,
        'event' => 0,
        'feed_ical' => 0,
        'feed_ical_item' => 0,
        'group' => 0,
        'profile' => 0,
        'feed' => 0,
        'feed_item' => 0,
        'image' => 0,
        'argomenti_immagini' => 0,
        'anagrafe_controparte' => 0,
        'articolo' => 0,
        'faq' => 0,
        'fattispecie_particolare' => 0,
        'newsfeed' => 0,
        'scheda_approfondimento' => 0,
        'scheda_didattica' => 0,
        'scheda_problemi' => 0,
        'casetracker_basic_case' => 0,
        'casetracker_basic_project' => 0,
        'panel' => 0,
        'alert' => 0,
        'associazione' => 0,
        'book' => 0,
        'domanda' => 0,
        'home_categoria' => 0,
        'home_statica' => 0,
        'landingpage' => 0,
        'page' => 0,
        'richiesta' => 0,
        'risposta' => 0,
        'risposta_associazione' => 0,
        'scheda_associazione' => 0,
        'simplenews' => 0,
      ),
      'validate_argument_node_access' => 0,
      'validate_argument_nid_type' => 'nid',
      'validate_argument_vocabulary' => array(
        '26' => 0,
        '27' => 0,
        '30' => 0,
        '10' => 0,
        '23' => 0,
        '21' => 0,
        '9' => 0,
        '6' => 0,
        '12' => 0,
        '11' => 0,
        '25' => 0,
        '24' => 0,
        '29' => 0,
        '31' => 0,
        '28' => 0,
      ),
      'validate_argument_type' => 'tid',
      'validate_argument_transform' => 0,
      'validate_user_restrict_roles' => 0,
      'validate_argument_node_flag_name' => '*relationship*',
      'validate_argument_node_flag_test' => 'flaggable',
      'validate_argument_node_flag_id_type' => 'id',
      'validate_argument_user_flag_name' => '*relationship*',
      'validate_argument_user_flag_test' => 'flaggable',
      'validate_argument_user_flag_id_type' => 'id',
      'image_size' => array(
        '_original' => '_original',
        'thumbnail' => 'thumbnail',
        'preview' => 'preview',
      ),
      'validate_argument_is_member' => 'OG_VIEWS_DO_NOT_VALIDATE_MEMBERSHIP',
      'validate_argument_group_node_type' => array(
        'group' => 0,
        'associazione' => 0,
      ),
      'validate_argument_php' => '',
    ),
  ));
  $handler->override_option('filters', array(
    'type' => array(
      'operator' => 'in',
      'value' => array(
        'richiesta' => 'richiesta',
      ),
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'id' => 'type',
      'table' => 'node',
      'field' => 'type',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('access', array(
    'type' => 'none',
  ));
  $handler->override_option('cache', array(
    'type' => 'none',
  ));
  $handler->override_option('use_ajax', TRUE);
  $handler->override_option('use_pager', '1');
  $handler->override_option('style_plugin', 'table');
  $handler->override_option('style_options', array(
    'grouping' => '',
    'override' => 1,
    'sticky' => 0,
    'order' => 'desc',
    'columns' => array(
      'title' => 'title',
      'body' => 'body',
      'created' => 'created',
      'state' => 'state',
      'view_node' => 'view_node',
      'type' => 'type',
    ),
    'info' => array(
      'title' => array(
        'sortable' => 0,
        'separator' => '',
      ),
      'body' => array(
        'separator' => '',
      ),
      'created' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'state' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'view_node' => array(
        'separator' => '',
      ),
      'type' => array(
        'sortable' => 0,
        'separator' => '',
      ),
    ),
    'default' => '-1',
  ));
  $handler = $view->new_display('page', 'Pagina', 'page_1');
  $handler->override_option('path', 'user/%/le_mie_richieste');
  $handler->override_option('menu', array(
    'type' => 'tab',
    'title' => 'Le mie richieste',
    'description' => 'Tutte le mie richieste',
    'weight' => '0',
    'name' => 'navigation',
  ));
  $handler->override_option('tab_options', array(
    'type' => 'none',
    'title' => '',
    'description' => '',
    'weight' => 0,
    'name' => 'navigation',
  ));

  $views[$view->name] = $view;

  // Exported view: scheda_rilevazione_richiesta
  $view = new view;
  $view->name = 'scheda_rilevazione_richiesta';
  $view->description = '';
  $view->tag = 'Segnalazione online > Scheda rilevazione';
  $view->view_php = '';
  $view->base_table = 'node';
  $view->is_cacheable = FALSE;
  $view->api_version = 2;
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
  $handler = $view->new_display('default', 'Defaults', 'default');
  $handler->override_option('fields', array(
    'body' => array(
      'label' => 'Testo',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '_blank',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
        'strip_tags' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'exclude' => 0,
      'id' => 'body',
      'table' => 'node_revisions',
      'field' => 'body',
      'relationship' => 'none',
    ),
    'state' => array(
      'label' => 'Stato attuale della richiesta',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
        'strip_tags' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'exclude' => 0,
      'id' => 'state',
      'table' => 'workflow_states',
      'field' => 'state',
      'relationship' => 'none',
    ),
    'path' => array(
      'label' => 'Collegamento contenuto',
      'alter' => array(
        'alter_text' => 1,
        'text' => 'Vai al contnuto della Richiesta',
        'make_link' => 1,
        'path' => '[path]',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '_blank',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
        'strip_tags' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'absolute' => 1,
      'exclude' => 0,
      'id' => 'path',
      'table' => 'node',
      'field' => 'path',
      'override' => array(
        'button' => 'Override',
      ),
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('arguments', array(
    'nid' => array(
      'default_action' => 'default',
      'style_plugin' => 'default_summary',
      'style_options' => array(),
      'wildcard' => 'all',
      'wildcard_substitution' => 'All',
      'title' => '',
      'breadcrumb' => '',
      'default_argument_type' => 'node',
      'default_argument' => '',
      'validate_type' => 'none',
      'validate_fail' => 'not found',
      'break_phrase' => 0,
      'not' => 0,
      'id' => 'nid',
      'table' => 'node',
      'field' => 'nid',
      'validate_user_argument_type' => 'uid',
      'validate_user_roles' => array(
        '2' => 0,
        '14' => 0,
        '9' => 0,
        '11' => 0,
        '12' => 0,
        '13' => 0,
        '5' => 0,
        '16' => 0,
        '10' => 0,
        '4' => 0,
        '3' => 0,
        '15' => 0,
        '6' => 0,
        '8' => 0,
        '7' => 0,
      ),
      'relationship' => 'none',
      'default_options_div_prefix' => '',
      'default_argument_fixed' => '',
      'default_argument_user' => 0,
      'default_argument_image_size' => '_original',
      'default_argument_php' => '',
      'validate_argument_node_type' => array(
        'blog' => 0,
        'event' => 0,
        'feed_ical' => 0,
        'feed_ical_item' => 0,
        'group' => 0,
        'profile' => 0,
        'feed' => 0,
        'feed_item' => 0,
        'image' => 0,
        'argomenti_immagini' => 0,
        'anagrafe_controparte' => 0,
        'articolo' => 0,
        'faq' => 0,
        'fattispecie_particolare' => 0,
        'newsfeed' => 0,
        'scheda_approfondimento' => 0,
        'scheda_didattica' => 0,
        'scheda_problemi' => 0,
        'casetracker_basic_case' => 0,
        'casetracker_basic_project' => 0,
        'panel' => 0,
        'alert' => 0,
        'associazione' => 0,
        'book' => 0,
        'domanda' => 0,
        'home_categoria' => 0,
        'home_statica' => 0,
        'landingpage' => 0,
        'page' => 0,
        'richiesta' => 0,
        'risposta_associazione' => 0,
        'scheda_associazione' => 0,
        'simplenews' => 0,
      ),
      'validate_argument_node_access' => 0,
      'validate_argument_nid_type' => 'nid',
      'validate_argument_vocabulary' => array(
        '26' => 0,
        '27' => 0,
        '30' => 0,
        '10' => 0,
        '23' => 0,
        '21' => 0,
        '9' => 0,
        '6' => 0,
        '12' => 0,
        '11' => 0,
        '25' => 0,
        '24' => 0,
        '29' => 0,
        '31' => 0,
        '28' => 0,
      ),
      'validate_argument_type' => 'tid',
      'validate_argument_transform' => 0,
      'validate_user_restrict_roles' => 0,
      'validate_argument_node_flag_name' => '*relationship*',
      'validate_argument_node_flag_test' => 'flaggable',
      'validate_argument_node_flag_id_type' => 'id',
      'validate_argument_user_flag_name' => '*relationship*',
      'validate_argument_user_flag_test' => 'flaggable',
      'validate_argument_user_flag_id_type' => 'id',
      'image_size' => array(
        '_original' => '_original',
        'thumbnail' => 'thumbnail',
        'preview' => 'preview',
      ),
      'validate_argument_is_member' => 'OG_VIEWS_DO_NOT_VALIDATE_MEMBERSHIP',
      'validate_argument_group_node_type' => array(
        'group' => 0,
        'associazione' => 0,
      ),
      'validate_argument_php' => '',
    ),
  ));
  $handler->override_option('filters', array(
    'type' => array(
      'operator' => 'in',
      'value' => array(
        'richiesta' => 'richiesta',
      ),
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'id' => 'type',
      'table' => 'node',
      'field' => 'type',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('access', array(
    'type' => 'none',
  ));
  $handler->override_option('cache', array(
    'type' => 'none',
  ));
  $handler->override_option('title', 'Richiesta');
  $handler->override_option('header', '<strong>Richiesta</strong>');
  $handler->override_option('header_format', '3');
  $handler->override_option('header_empty', 0);
  $handler->override_option('items_per_page', 1);
  $handler->override_option('row_options', array(
    'inline' => array(),
    'separator' => '',
    'hide_empty' => 0,
  ));
  $handler = $view->new_display('node_content', 'Node content', 'node_content_1');
  $handler->override_option('types', array(
    'scheda_problemi' => 'scheda_problemi',
  ));
  $handler->override_option('modes', array(
    '0' => 'full',
  ));
  $handler->override_option('argument_mode', 'token');
  $handler->override_option('default_argument', '[field_scheda_problema_richie-nid]');
  $handler->override_option('show_title', 0);

  $views[$view->name] = $view;

  // Exported view: scheda_rilevazione_risposta
  $view = new view;
  $view->name = 'scheda_rilevazione_risposta';
  $view->description = '';
  $view->tag = 'Segnalazione online > Scheda rilevazione';
  $view->view_php = '';
  $view->base_table = 'node';
  $view->is_cacheable = FALSE;
  $view->api_version = 2;
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
  $handler = $view->new_display('default', 'Defaults', 'default');
  $handler->override_option('fields', array(
    'body' => array(
      'label' => 'Testo',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '_blank',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
        'strip_tags' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'exclude' => 0,
      'id' => 'body',
      'table' => 'node_revisions',
      'field' => 'body',
      'relationship' => 'none',
    ),
    'state' => array(
      'label' => 'Stato attuale della risposta',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
        'strip_tags' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'exclude' => 0,
      'id' => 'state',
      'table' => 'workflow_states',
      'field' => 'state',
      'relationship' => 'none',
    ),
    'path' => array(
      'label' => 'Collegamento contenuto',
      'alter' => array(
        'alter_text' => 1,
        'text' => 'Vai al contenuto di Risposta',
        'make_link' => 1,
        'path' => '[path]',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '_blank',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
        'strip_tags' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'absolute' => 1,
      'exclude' => 0,
      'id' => 'path',
      'table' => 'node',
      'field' => 'path',
      'relationship' => 'none',
    ),
    'edit_node' => array(
      'label' => 'Modifica risposta',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
        'strip_tags' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'text' => '',
      'exclude' => 0,
      'id' => 'edit_node',
      'table' => 'node',
      'field' => 'edit_node',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('arguments', array(
    'field_risposta_rilevazione_refer_nid' => array(
      'default_action' => 'not found',
      'style_plugin' => 'default_summary',
      'style_options' => array(),
      'wildcard' => 'all',
      'wildcard_substitution' => 'All',
      'title' => '',
      'breadcrumb' => '',
      'default_argument_type' => 'fixed',
      'default_argument' => '',
      'validate_type' => 'none',
      'validate_fail' => 'not found',
      'break_phrase' => 0,
      'not' => 0,
      'id' => 'field_risposta_rilevazione_refer_nid',
      'table' => 'node_data_field_risposta_rilevazione_refer',
      'field' => 'field_risposta_rilevazione_refer_nid',
      'validate_user_argument_type' => 'uid',
      'validate_user_roles' => array(
        '2' => 0,
        '14' => 0,
        '9' => 0,
        '11' => 0,
        '12' => 0,
        '13' => 0,
        '5' => 0,
        '16' => 0,
        '10' => 0,
        '4' => 0,
        '3' => 0,
        '15' => 0,
        '6' => 0,
        '8' => 0,
        '7' => 0,
      ),
      'relationship' => 'none',
      'default_options_div_prefix' => '',
      'default_argument_fixed' => '',
      'default_argument_user' => 0,
      'default_argument_image_size' => '_original',
      'default_argument_php' => '',
      'validate_argument_node_type' => array(
        'blog' => 0,
        'event' => 0,
        'feed_ical' => 0,
        'feed_ical_item' => 0,
        'group' => 0,
        'profile' => 0,
        'feed' => 0,
        'feed_item' => 0,
        'image' => 0,
        'argomenti_immagini' => 0,
        'anagrafe_controparte' => 0,
        'articolo' => 0,
        'faq' => 0,
        'fattispecie_particolare' => 0,
        'newsfeed' => 0,
        'scheda_approfondimento' => 0,
        'scheda_didattica' => 0,
        'scheda_problemi' => 0,
        'casetracker_basic_case' => 0,
        'casetracker_basic_project' => 0,
        'panel' => 0,
        'alert' => 0,
        'associazione' => 0,
        'book' => 0,
        'domanda' => 0,
        'home_categoria' => 0,
        'home_statica' => 0,
        'landingpage' => 0,
        'page' => 0,
        'richiesta' => 0,
        'risposta' => 0,
        'risposta_associazione' => 0,
        'scheda_associazione' => 0,
        'simplenews' => 0,
      ),
      'validate_argument_node_access' => 0,
      'validate_argument_nid_type' => 'nid',
      'validate_argument_vocabulary' => array(
        '26' => 0,
        '27' => 0,
        '30' => 0,
        '10' => 0,
        '23' => 0,
        '21' => 0,
        '9' => 0,
        '6' => 0,
        '12' => 0,
        '11' => 0,
        '25' => 0,
        '24' => 0,
        '29' => 0,
        '31' => 0,
        '28' => 0,
      ),
      'validate_argument_type' => 'tid',
      'validate_argument_transform' => 0,
      'validate_user_restrict_roles' => 0,
      'validate_argument_node_flag_name' => '*relationship*',
      'validate_argument_node_flag_test' => 'flaggable',
      'validate_argument_node_flag_id_type' => 'id',
      'validate_argument_user_flag_name' => '*relationship*',
      'validate_argument_user_flag_test' => 'flaggable',
      'validate_argument_user_flag_id_type' => 'id',
      'image_size' => array(
        '_original' => '_original',
        'thumbnail' => 'thumbnail',
        'preview' => 'preview',
      ),
      'validate_argument_is_member' => 'OG_VIEWS_DO_NOT_VALIDATE_MEMBERSHIP',
      'validate_argument_group_node_type' => array(
        'group' => 0,
        'associazione' => 0,
      ),
      'validate_argument_php' => '',
    ),
  ));
  $handler->override_option('filters', array(
    'type' => array(
      'operator' => 'in',
      'value' => array(
        'risposta' => 'risposta',
      ),
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'id' => 'type',
      'table' => 'node',
      'field' => 'type',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('access', array(
    'type' => 'none',
  ));
  $handler->override_option('cache', array(
    'type' => 'none',
  ));
  $handler->override_option('title', 'Risposta');
  $handler->override_option('header', '<strong>Risposta</strong>');
  $handler->override_option('header_format', '3');
  $handler->override_option('header_empty', 0);
  $handler->override_option('empty', 'Nessuna risposta è stata ancora creata');
  $handler->override_option('empty_format', '1');
  $handler->override_option('items_per_page', 1);
  $handler->override_option('row_options', array(
    'inline' => array(),
    'separator' => '',
    'hide_empty' => 0,
  ));
  $handler = $view->new_display('node_content', 'Node content', 'node_content_1');
  $handler->override_option('types', array(
    'scheda_problemi' => 'scheda_problemi',
  ));
  $handler->override_option('modes', array(
    '0' => 'full',
  ));
  $handler->override_option('argument_mode', 'nid');
  $handler->override_option('show_title', 0);

  $views[$view->name] = $view;

  return $views;
}
